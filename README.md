# RF Switch Matrix IOC

This repository contains RF Switch Matrix IOC code.

Hardware includes AK-NORD XT Pico on the Ethernet module PCB. IOC works with
on-board TCA9555 I/O I2C chip that drives 5 I/O lines to control the digital
switch.

Control of the digital switch is made through 16 states (State-0 .. State-16),
which are binary encoded using 5 bits (values from 0 .. 16).
Each state is represented by an EPICS PV that in turn drives the TCA9555 pin
level to either low of high. All the pins of the TCA9555 are driven at the
same time.

OPI screenshot

![OPI](/docs/opi1.png)

## Database

This IOC defines following PVs. For the XT Pico PVs see the accompanying GIT
repository.

| PV name | Description |
|---------|-------------|
| $(P)$(R)Init | Initializes I/O pin direction to output and level to low (State 0). This  PV is processed automatically at IOC startup. |
| $(P)$(R)State-RB | Holds currently selected state. |
| $(P)$(R)State-n | Select desired state; 0 <= n <= 16 |

## IOC startup

IOC startup file `st.cmd` loads the required database files for XT Pico TCA9555
control and RF switch matrix specific state PVs.

Following macros in the `st.cmd` file can be changed.

| Macro | Default | Description |
|-------|---------|-------------|
| LOCATION | LAB | Part of the PV prefix ① |
| DEVICE_NAME | RFSW-001 | Part of the PV prefix ① |
| DEVICE_IP | 172.30.150.48 | XT Pico IP address or DNS name |
| FIRST_PIN | 0 | First pin on TCA9555 used for RF swich matrix control |

① PV prefix is defined as `$(LOCATION):$(DEVICE_NAME):` by default.

In order to control several RF switch matrix systems one should create additional
`st.cmd` commands with above macros changed such that there are no name collisions.

Other macros or values should be left intact since they relate to XT Pico device
on the Ethernet module (and can not be changed for this application). These are
listed in the table below.

| Property | Value | Description |
|----------|---------|-------------|
| IP port | 1002 | TCP port for controlling first port on XT Pico |
| TCA9555 I2C address | 0x21 | I2C address of the TCA9555 on Ethernet module PCB |
