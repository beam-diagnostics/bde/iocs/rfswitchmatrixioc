< envVars

errlogInit(20000)

dbLoadDatabase("$(TOP_DIR)/dbd/$(APP).dbd")
$(APP)_registerRecordDeviceDriver(pdbbase) 

#- 1 MB max CA request
epicsEnvSet("EPICS_CA_MAX_ARRAY_BYTES",     "1000000")
epicsEnvSet("LOCATION",                     "LAB")
epicsEnvSet("DEVICE_IP",                    "172.30.150.48")
epicsEnvSet("DEVICE_NAME",                  "RFSW-001")
epicsEnvSet("PREFIX",                       "$(LOCATION):$(DEVICE_NAME):")
epicsEnvSet("FIRST_PIN",                    "0")
#- asyn IP comm ports
epicsEnvSet("I2C_COMM_PORT",                "I2C_COMM")

#- Create the asyn port to talk XTpico PORT1; TCP port 1002.
drvAsynIPPortConfigure($(I2C_COMM_PORT),"$(DEVICE_IP):1002")
#- asynSetTraceIOMask("$(I2C_COMM_PORT)",0,255)
#- asynSetTraceMask("$(I2C_COMM_PORT)",0,255)

#- one port expander
iocshLoad("../iocsh/tca9555.iocsh", "IP_PORT=$(I2C_COMM_PORT), N=1, NAME=IO1, COUNT=1, INFOS=0x21")

#- RF switch matrix database
dbLoadRecords("rf-switch-matrix.db", "P=$(PREFIX),R=,IO=IO1-,SHIFT=$(FIRST_PIN)")

set_requestfile_path("$(DB_DIR)")
set_requestfile_path("./")
set_savefile_path("$(AUTOSAVE_DIR)")

set_pass0_restoreFile("auto_settings.sav")
set_pass1_restoreFile("auto_settings.sav")

save_restoreSet_status_prefix("$(PREFIX)")
dbLoadRecords("save_restoreStatus.db","P=$(PREFIX)")

###############################################################################
iocInit
###############################################################################

#- save things every thirty seconds
create_monitor_set("auto_settings.req",30,"P=$(PREFIX),R=")


date
###############################################################################
