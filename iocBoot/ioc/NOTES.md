# XT pico configuration for RF Switch Matrix


    =========================== INTERFACE MENU ==================================

      1 = I2C1 Menu
      2 = SPI2 Menu
      E = ETHERNET Menu

















      For example:'A'[ENTER]
    -----------------------------------------------------------------------------
    [Q = QUIT] Please enter your choice:

## PORT1

    =========================== I2C CONFIG MENU =================================

      1 = Slave Addr        = 0
      2 = Baudrate          = 100000
      3 = Data Control      = P
      4 = Data ready timeout= 10(*10ms)

      5 = Flow Control      = N
      6 = RTS Protocol      = 0

      a = Emulation         = TCPSERVER
      b = EmuCode           = 0000
      c = BUS               = I2C
      d = InputTimeOut*10ms = 0
      e = Local Port        = 1002
      f = With SSL/TLS      = N


      STATE=HW ONLINE

      RTS = LOW   CTS = LOW 

      For example:'1=2'
    -----------------------------------------------------------------------------
    [Q = QUIT] Please enter your choice:

